#!/usr/bin/env bash

set -e

USER="${USER:-abc}"
PASSWORD="${PASSWORD:-123}"
GROUP="${GROUP:-def}"
USER_ID="${USER_ID:-2500}"
GROUP_ID="${GROUP_ID:-2500}"
SHARE="${SHARE:-/share}"

echo "Creating group '${GROUP}' with id ${GROUP_ID}"
addgroup -g ${GROUP_ID} "${GROUP}"

echo "Creating user '${USER}' with id ${USER_ID}"
adduser -D -u ${USER_ID} -G "${GROUP}" ${USER}

echo "Setting password for user '${USER}'"

chpasswd <<<"${USER}:${PASSWORD}"

mkdir -p "${SHARE}"

if ! test -f "/etc/ssh/ssh_host_rsa_key"; then
    echo "No host keys. Running ssh-keygen -A"
    ssh-keygen -A
fi

echo 'PermitEmptyPasswords yes' >> /etc/ssh/sshd_config
echo "Match User ${USER}
    ChrootDirectory ${SHARE}
    ForceCommand internal-sftp
    AllowTcpForwarding no" >> /etc/ssh/sshd_config

echo "Starting SSH daemon ..."

/usr/sbin/sshd -D -e
